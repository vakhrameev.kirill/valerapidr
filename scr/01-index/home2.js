a = 13.890123
b = 2.891564
n = 3

let CentrA = a - Math.trunc(a);
let DrobA = Math.trunc(CentrA * Math.pow(10,n));

let CentrB = b - Math.trunc(b);
let DrobB = Math.trunc(CentrB * Math.pow(10,n));

console.log(DrobA, DrobB);
console.log(DrobA === DrobB);
console.log(DrobA > DrobB);
console.log(DrobA < DrobB);
console.log(DrobA <= DrobB);
console.log(DrobA >= DrobB);
console.log(DrobA !== DrobB);